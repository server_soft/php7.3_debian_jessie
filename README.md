# Docker build
docker build -t php7.3_debian_jessie .

# Extensions path
/etc/php/7.3/mods-available

# Search extensions
apt-cache search php
apt-cache search php7.*
apt-cache search php7.3-*

# Extensions
- calendar.ini
- ctype.ini
- curl.ini
- exif.ini
- fileinfo.ini
- ftp.ini
- gettext.ini
- iconv.ini
- json.ini
- opcache.ini
- pdo.ini
- phar.ini
- posix.ini
- readline.ini
- shmop.ini
- sockets.ini
- sysvmsg.ini
- sysvsem.ini
- sysvshm.ini
- tokenizer.ini

# php -m
root@a7e742c88bd3:/srv# php -m
[PHP Modules]
- calendar
- Core
- ctype
- curl
- date
- exif
- fileinfo
- filter
- ftp
- gettext
- hash
- iconv
- json
- libxml
- openssl
- pcntl
- pcre
- PDO
- Phar
- posix
- readline
- Reflection
- session
- shmop
- sockets
- sodium
- SPL
- standard
- sysvmsg
- sysvsem
- sysvshm
- tokenizer
- Zend OPcache
- zlib

[Zend Modules]
- Zend OPcache

# All packages
- php7.3 - server-side, HTML-embedded scripting language (metapackage)
- php7.3-bcmath - Bcmath module for PHP
- php7.3-bz2 - bzip2 module for PHP
- php7.3-cgi - server-side, HTML-embedded scripting language (CGI binary)
- php7.3-cli - command-line interpreter for the PHP scripting language
- php7.3-common - documentation, examples and common module for PHP
- php7.3-curl - CURL module for PHP
- php7.3-dba - DBA module for PHP
- php7.3-dev - Files for PHP7.3 module development
- php7.3-enchant - Enchant module for PHP
- php7.3-fpm - server-side, HTML-embedded scripting language (FPM-CGI binary)
- php7.3-gd - GD module for PHP
- php7.3-gmp - GMP module for PHP
- php7.3-imap - IMAP module for PHP
- php7.3-interbase - Interbase module for PHP
- php7.3-intl - Internationalisation module for PHP
- php7.3-json - JSON module for PHP
- php7.3-ldap - LDAP module for PHP
- php7.3-mbstring - MBSTRING module for PHP
- php7.3-mysql - MySQL module for PHP
- php7.3-odbc - ODBC module for PHP
- php7.3-opcache - Zend OpCache module for PHP
- php7.3-pgsql - PostgreSQL module for PHP
- php7.3-phpdbg - server-side, HTML-embedded scripting language (PHPDBG binary)
- php7.3-pspell - pspell module for PHP
- php7.3-readline - readline module for PHP
- php7.3-recode - recode module for PHP
- php7.3-snmp - SNMP module for PHP
- php7.3-soap - SOAP module for PHP
- php7.3-sqlite3 - SQLite3 module for PHP
- php7.3-sybase - Sybase module for PHP
- php7.3-tidy - tidy module for PHP
- php7.3-xml - DOM, SimpleXML, WDDX, XML, and XSL module for PHP
- php7.3-xmlrpc - XMLRPC-EPI module for PHP
- php7.3-xsl - XSL module for PHP (dummy)
- php7.3-zip - Zip module for PHP
